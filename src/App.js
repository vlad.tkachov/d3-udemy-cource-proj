import React, {useEffect, useState} from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import ChartWrapper from "./components/ChartWrapper";
import Navbar from "react-bootstrap/Navbar";
import Container from "react-bootstrap/Container";
import Table from './components/Table'
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { json } from "d3";

const url = 'https://udemy-react-d3.firebaseio.com/children.json';


function App() {

    const [data, setData] = useState([]);
    const [activeStudent, setActiveStudent] = useState(null);

    useEffect(() => {
        json(url)
            .then(setData)
            .catch((e) => console.log('Some think went wrong', e))
    }, []);

  return (
      <>
      <Navbar bg="light">
        <Navbar.Brand>Brand text</Navbar.Brand>
      </Navbar>
        <Container>
          <Row>
              <Col xs={12} md={6}>
                  <ChartWrapper data={data} setActiveStudent={setActiveStudent}/>
              </Col>
              <Col xs={12} md={6}>
                  <Table students={data} setData={setData} activeStudent={activeStudent}/>
              </Col>
          </Row>
        </Container>
    </>
  );
}

export default App;
