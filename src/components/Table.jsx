import React, {useState} from "react";
import {Button, Col, Form, Row} from "react-bootstrap";

const initialFormValue = {name: '', age: '', height: ''};

const Table = ({students, setData, activeStudent}) => {
    const [form, setForm] = useState({...initialFormValue});

    const addStudent = () => {
        console.log(form);
        setData([...students, form]);
        setForm({...initialFormValue});
    };

    const updateField = (field, value) => {
        setForm({...form, [field]: value})
    };

    const removeStudent = (name) => {
        setData(students.filter(student => student.name !== name));
    };

    const Rows = students.map((student, i) => <Row
        style={{'backgroundColor': activeStudent === student.name ? 'gray' : 'white'}} key={i} className="py-2">
        <Col xs={3}>{student.name}</Col>
        <Col xs={3}>{student.height}</Col>
        <Col xs={3}>{student.age}</Col>
        <Col xs={3}>
            <Button
                variant="danger"
                type="button"
                style={{'width': '100%'}}
                onClick={() => removeStudent(student.name)}
            >
                Remove
            </Button>
        </Col>
    </Row>);

    return <>
        <Row className="py-2">
            <Col xs={3}>
                <Form.Control placeholder="Name" name="name" value={form.name}
                              onChange={(e) => updateField('name', e.target.value)}/>
            </Col>
            <Col xs={3}>
                <Form.Control placeholder="Height" name="height" value={form.height}
                              onChange={(e) => updateField('height', e.target.value)}/>
            </Col>
            <Col xs={3}>
                <Form.Control placeholder="Age" name="age" value={form.age}
                              onChange={(e) => updateField('age', e.target.value)}/>
            </Col>
            <Col>
                <Button
                    variant="primary"
                    type="button"
                    style={{'width': '100%'}}
                    onClick={addStudent}
                >
                    Add
                </Button>
            </Col>
        </Row>
        {Rows}
    </>;
};

export default Table;
