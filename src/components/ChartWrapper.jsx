import React, {useEffect, useLayoutEffect, useRef, useState} from "react";
import D3Chart from "./D3Chart";

const ChartWrapper = ({data, setActiveStudent}) => {
    const chartRef = useRef(null);
    const [chart, setChart] = useState(null);

    useEffect(() => {
        setChart(new D3Chart(chartRef.current, data, setActiveStudent))
    }, []);

    useEffect(() => {
        if (chart) {
            chart.update(data)
        }
    }, [data]);

    return <div ref={chartRef}/>
};

export default ChartWrapper;
