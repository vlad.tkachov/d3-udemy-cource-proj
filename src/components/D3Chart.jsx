import * as d3 from "d3";

const MARGIN = {top: 10, left: 70, bottom: 50, right: 10};
const HEIGHT = 300 - MARGIN.top - MARGIN.bottom;
const WIDTH = 500 - MARGIN.left - MARGIN.right;

export default class D3Chart {
    g = null;
    x = null;
    y = null;
    xAxisGroup = null;
    yAxisGroup = null;
    setActiveName = null;

    constructor(element, data, setActiveName) {
        this.setActiveName = setActiveName;
        this.g = d3.select(element)
            .append("svg")
            .attr("width", WIDTH + MARGIN.left + MARGIN.right)
            .attr("height", HEIGHT + MARGIN.top + MARGIN.bottom)
            .append("g")
            .attr("transform", `translate(${MARGIN.left}, ${MARGIN.top})`);

        this.x = d3.scaleLinear()
            .range([0, WIDTH]);

        this.y = d3.scaleLinear()
            .range([HEIGHT, 0]);

        this.xAxisGroup = this.g.append('g')
            .attr('transform', `translate(0, ${HEIGHT})`);

        this.yAxisGroup = this.g.append('g');

        this.g.append("text")
            .attr("x", WIDTH / 2)
            .attr("y", HEIGHT + 40)
            .attr("text-anchor", "middle")
            .text("Age");

        this.g.append("text")
            .attr("x", -(HEIGHT / 2))
            .attr("y", -50)
            .attr("text-anchor", "middle")
            .attr("transform", "rotate(-90)")
            .text("Height in cm");

        this.update(data);
    }

    update = data => {
        const maxAge = d3.max(data, d => +d.age);
        const maxHeight = d3.max(data, d => +d.height);

        this.x.domain([0, maxAge]);
        this.y.domain([0, maxHeight]);

        const xAxisCall = d3.axisBottom(this.x);
        const yAxisCall = d3.axisLeft(this.y);

        this.xAxisGroup.transition(1000).call(xAxisCall);
        this.yAxisGroup.transition(1000).call(yAxisCall);

        const circles = this.g.selectAll('circle')
            .data(data, d => d.name);

        circles.exit().transition(1000).attr('cy', this.y(0)).remove();

        circles
            .transition(1000)
            .attr('cx', d => this.x(+d.age))
            .attr('cy', d => {
                console.log(+d.height);
                return this.y(+d.height)
            });

        circles.enter().append('circle')
            .attr('cy', this.y(0))
            .attr('cx', d => this.x(+d.age))
            .attr('r', 5)
            .attr('fill', 'gray')
            .on('click', d => this.setActiveName(d.name))
            .transition(1000)
            .attr('cy', d => this.y(+d.height))

    }
}
